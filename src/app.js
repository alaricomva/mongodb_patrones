const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();

//settings
app.set('port', process.env.PORT || 3001);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//mongoose
mongoose.connect('mongodb+srv://alarico:alarico1@patrones-y1zlz.mongodb.net/test')
.then(db => console.log('db conectada'))
.catch(err => console.log(err));

// import routes
const indexRoutes = require('./routes/index');

app.use(cors());
//middlewares
app.use(express.urlencoded({extended: false}));

//routes
app.use('/', indexRoutes);
app.use(cors());

app.listen(app.get('port'), () => {
  console.log(app.get('port'));
});
