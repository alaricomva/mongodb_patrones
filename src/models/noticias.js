const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const taskSchema = new Schema({
    id: Number,
    titulo: String,
    autor: String,
    contenido: String,
    imagen: String,
    fecha: Date,
    likes: Number,
    visitas: Number,
    comentarios: [{
        id: Number,
        contenido: String,
        fecha: Date,
        usuario: String,
        respuesta: [{
            id: Number,
            contenido: String,
            usuario: String
        }]
    }]

});

module.exports = mongoose.model('noticias',taskSchema);


