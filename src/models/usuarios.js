const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const bcrypt = require('bcrypt')

var SALT_WORK_FACTOR = 10;

var taskSchema = new Schema({
    usuario_id: Number,
    nombre: String,
    apellido: String,
    email: {
        type: String, 
        unique: true
    },
    admin: {
        type:Boolean,
        default: false
    },
    confirmado: {
        type:Boolean,
        default: false
    },
    clave: String
});

taskSchema.pre('save',function(next){

var user = this;

  if (!user.isModified('clave')) return next();

  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
      if (err) return next(err);


      bcrypt.hash(user.clave, salt, function(err, hash) {
          if (err) return next(err);


          user.clave = hash;
          next();
      });
  });
});

taskSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.clave, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('usuarios',taskSchema);


