const express = require('express');
const router = express.Router();
const Task = require('../models/task');
const Noticias = require('../models/noticias');
const Usuarios = require('../models/usuarios')
bodyParser = require('body-parser').json();
const jwt = require('jsonwebtoken');

var currentuserid;
router.get('/', async (req,res) => {
    Noticias.count({},function(err,result){
        console.log(result);
        const idnueva = result+1;
    });

    const noticiamod = await Usuarios.find();
    console.log(noticiamod);
 
    const noticias = await Noticias.find();
    console.log(noticias);
    res.render('index',{
        noticias
    });
})

router.post('/usuarioscompleto', bodyParser, async(req,res)=>{
    const todosusuarios = await Usuarios.find();
    console.log(todosusuarios);
    return res.send(todosusuarios)
})


router.post('/todasnoticias', bodyParser, async(req,res)=>{
    const noticias = await Noticias.find();
    console.log(noticias);
    const fecha = Date.now()
    console.log(fecha)
    return res.send(noticias)
})

router.post('/updatestatus',bodyParser,async(req,res)=>{
    Usuarios.findOneAndUpdate({usuario_id: req.body.usuario_id},{$set:{confirmado: true}},{new:true},(err,doc)=>{
        console.log(doc)
        return res.send(doc)
    });
})

router.post('/views', bodyParser, async(req,res)=>{
    console.log("entra")
    Noticias.findOneAndUpdate({id: req.body.id},{$set:{visitas: req.body.visitas}},{new:true},(err,doc)=>{
        console.log(doc)
        return res.send(doc)
    });
    
    
})

router.post('/nuevasnoticias', bodyParser, async(req,res)=>{
    const noticias = await Noticias.find().sort({fecha: -1});
    console.log(noticias);
    return res.send(noticias)
})

router.post('/unanoticia', bodyParser, async(req,res)=>{
    const noticias = await Noticias.findOne({id:req.body.id});
    console.log(noticias);
    return res.send(noticias)
})

router.post('/buscarnombre', bodyParser, async(req,res)=>{
    const noticias = await Noticias.find({titulo: {$regex: req.body.titulo, $options: "i"}});
    console.log(noticias);
    return res.send(noticias)
})

router.post('/add',bodyParser,verifyToken, async (req,res) => {

    const idusuario = req.userid;

    const usuariobusqueda = await Usuarios.findOne({usuario_id: idusuario});
    const nombrecompleto = usuariobusqueda.nombre + ' ' + usuariobusqueda.apellido;

    const todos = await Noticias.find();
    const contador = todos.length + 1;


    const noticia = new Noticias({id: contador, titulo: req.body.titulo,autor: nombrecompleto, contenido: req.body.contenido, imagen: req.body.imagen, fecha: Date.now(), likes: 0, visitas: 0, comentarios:[]});
    await noticia.save();

     
    console.log(req.body);
    return res.send(noticia);
});

router.post('/signin',bodyParser, async (req,res) => {

    console.log(req.userid)

    Usuarios.findOne({email: req.body.email}, function(err,user){
        if(!user){
            return res.send("No es usuario")
        } 
        console.log(user);
        user.comparePassword(req.body.clave, function(err,isMatch){
            if(isMatch){
                const token = jwt.sign({id: user.usuario_id}, 'secretkey')
                return res.send({token: token, confirmado: user.confirmado})
            }
            else{
                return res.send("Contraseña incorrecta")
            }
        })
    })

});

router.post('/admin',bodyParser, verifyToken, async (req,res) => {

    console.log(req.userid)

    const id = req.userid;

    Usuarios.findOne({usuario_id: id}, function(err,user){
        return res.send(user.admin)
    })

});

router.post('/likes',bodyParser, verifyToken, async (req,res) => {

    console.log("entra")
    Noticias.findOneAndUpdate({id: req.body.id},{$set:{likes: req.body.likes}},{new:true},(err,doc)=>{
        console.log(doc)
        return res.send(doc)
    });

});


router.post('/adduser',bodyParser, async (req,res) => {

    console.log(req.body);

    var idnueva;

    Usuarios.count({},function(err,result){
        console.log(result);
        idnueva = result+1;
        console.log(idnueva);

    });
    const todos = await Usuarios.find();

    if(todos.length == 0){
        var newid = todos.length;
         newid = newid +1;
    }
    else{
        var tamano = todos.length;
        var newid = todos[tamano-1].usuario_id + 1;
    }

    


    const usuario = new Usuarios({usuario_id: newid, nombre: req.body.nombre,apellido: req.body.apellido, email: req.body.email, admin: false,confirmado:false, clave: req.body.clave});
    await usuario.save();

    const token = jwt.sign({id: usuario.usuario_id},'secretkey')

    const noticiamod = await Usuarios.find();
    console.log(noticiamod);
     
    console.log(req.body);
    return res.send(req.body);
});

router.post('/addadmin',bodyParser, async (req,res) => {

    console.log(req.body);

    var idnueva;

    Usuarios.count({},function(err,result){
        console.log(result);
        idnueva = result+1;
        console.log(idnueva);

    });
    const todos = await Usuarios.find();

    if(todos.length == 0){
        var newid = todos.length;
         newid = newid +1;
    }
    else{
        var tamano = todos.length;
        var newid = todos[tamano-1].usuario_id + 1;
    }



    const usuario = new Usuarios({usuario_id: newid, nombre: req.body.nombre,apellido: req.body.apellido, email: req.body.email, admin: true, confirmado: true, clave: req.body.clave});
    await usuario.save();

    const token = jwt.sign({id: usuario.usuario_id},'secretkey')

    const noticiamod = await Usuarios.find();
    console.log(noticiamod);
     
    console.log(req.body);
    return res.send(req.body);
});


router.post('/addcomment',bodyParser, verifyToken, async (req,res) => {

    console.log("entra")
    const idusuario = req.userid;
    const noticiamod = await Noticias.findOne({id: req.body.noticia_id});
    
    const usuarioscompleto = await Usuarios.find();
    console.log(usuarioscompleto);

    const usuariobusqueda = await Usuarios.findOne({usuario_id: idusuario});
    
    const nombrecompleto = usuariobusqueda.nombre + ' ' + usuariobusqueda.apellido;

    if(noticiamod.comentarios.length == 0){
        var newid = noticiamod.comentarios.length;
        newid = newid +1;
    }
    else{
        var tamano = noticiamod.comentarios.length;
        var newid = noticiamod.comentarios[tamano-1].id + 1;
    }


    
    
    noticiamod.comentarios.push({id: newid, contenido: req.body.contenido,fecha: Date.now(),usuario: nombrecompleto});

    const nuevo = await noticiamod.save();
    console.log(nuevo);
    
    
    return res.send('hola');
});


router.post('/addresponse',bodyParser, verifyToken, async (req,res) => {

    console.log("entra")
    const idusuario = req.userid;
    const noticiamod = await Noticias.findOne({id: req.body.noticia_id});
    
    const usuarioscompleto = await Usuarios.find();
    console.log(usuarioscompleto);

    const usuariobusqueda = await Usuarios.findOne({usuario_id: idusuario});
    
    const nombrecompleto = usuariobusqueda.nombre + ' ' + usuariobusqueda.apellido;

    var index = -1;
    for(var i = 0; i < noticiamod.comentarios.length; i++){
        if(noticiamod.comentarios[i].id == req.body.id){
            index = i;    
        }
    }
    
    
    noticiamod.comentarios[index].respuesta.push({id: 0, contenido: req.body.contenido,usuario: nombrecompleto});

    console.log(noticiamod.comentarios[index].respuesta)
    const nuevo = await noticiamod.save();
    console.log(nuevo);
    
    
    return res.send('hola');
});


router.post('/deletecomment',bodyParser, verifyToken, async (req,res) => {

    console.log("entra")
    const idusuario = req.userid;
    const noticiamod = await Noticias.findOne({id: req.body.noticia_id});
    
    var index = -1;
    console.log(noticiamod.comentarios)
    for(var i = 0; i < noticiamod.comentarios.length; i++){
        if(noticiamod.comentarios[i].id == req.body.id){
            index = i;    
        }
    }
    if(index >= 0){
        noticiamod.comentarios.splice(index,1);
    }
    
   
    //noticiamod.comentarios.push({id: newid, contenido: req.body.contenido,fecha: Date.now(),usuario: nombrecompleto});

    const nuevo = await noticiamod.save();
    console.log(nuevo);
    
    
    return res.send('hola');
});

router.post('/delete',bodyParser, async (req,res) => {

    await Noticias.remove({id: req.body.noticia_id});

    console.log(Noticias.find());
    return res.send('hola');
});

router.post('/deleteuser',bodyParser, async (req,res) => {

    await Usuarios.remove({usuario_id: req.body.usuario_id});

    console.log(Usuarios.find());
    return res.send('hola');
});

function verifyToken(req,res,next){

    
    if(!req.headers.authorization){
        return res.send("Unathorize")
    }

    const token = req.headers.authorization.split(' ')[1]

    if(token == null){
        return res.send("unathorize")
    }

    const payload = jwt.verify(token,'secretkey')

    console.log(payload)

    req.userid = payload.id;
    currentuserid = payload.id;
    next()
}


module.exports = router;