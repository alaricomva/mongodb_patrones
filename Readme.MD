# Patrones Hermosos (MongoDB)

Esta es la aplicación de backend del sitio web Patrones Hermosos. Desarollado con NodeJS, utilizando Express y Mongoose.

Se encarga de administrar el manejo de sesiones, los usuarios, administradores y las noticias. Existe otra aplicación dedicada a los diferentes registros del programa, diseñada igualmente con Express, pero utilizando PostgreSQL

## Instalación

Revisar que se tiene la última versión de NodeJS7

```
nodejs -v
```

Si no es la versión 12 actualizarla con los siugiente comandos en un ambiente Ubuntu 19.04/18.04/16.04

```
sudo apt update
sudo apt -y install curl dirmngr apt-transport-https lsb-release ca-certificates
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
```

Con la última versión ya se pueden ejecutar los comandos de instalación de npm

Dentro de la carpeta del rpoyecto correr el siguiente comando

```
npm install
```

## Ejecución

Para ejecutar el programa:

```
npm start
```

Si se desea cambiar de ubicación la base de datos, es necesario realizar el proceso necesario utilizando MongoAtlas y modificar el link dentro de la aplicación en el archivo app.js